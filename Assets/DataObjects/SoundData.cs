﻿using System;
using UnityEngine;

namespace Arkanoid2D
{
    [Serializable]
    public class SoundData
    {

        public string name;
        public SoundType type;
        public bool loop;

        public float volume;
        public float pitch;

        public AudioClip clip;
        public int loopTimes;
    }
}
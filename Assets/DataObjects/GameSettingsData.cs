﻿using System;

namespace Arkanoid2D
{
    [Serializable]
    public class GameSettingsData
    {
        public float musicVolume = 1.0f;
        public float soundEffectsVolume = 1.0f;
    }
}


﻿using UnityEngine;

namespace Arkanoid2D
{
    [CreateAssetMenu]

    public class SceneSettingsData : ScriptableObject
    {
        public string sceneName;
        public SoundData[] soundsData;
    }
}
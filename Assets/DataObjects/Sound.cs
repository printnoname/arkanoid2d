﻿using UnityEngine;

namespace Arkanoid2D
{
        public class Sound
        {

            public string name;
            public SoundType type;
            public bool loop;
            [HideInInspector]
            public SoundStatus status;

            public float volume;
            public float pitch;
            [HideInInspector]
            public int length;

            public AudioClip clip;
            [HideInInspector]
            public AudioSource[] sources;
            public int loopTimes;

            public Sound(SoundData soundData)
            {
                this.name = soundData.name;
                this.type = soundData.type;
                this.loop = soundData.loop;
                this.status = SoundStatus.NON_ACTIVE;
                this.volume = soundData.volume;
                this.pitch = soundData.pitch;
                this.clip = soundData.clip;
                this.loopTimes = soundData.loopTimes;
            }
        }


        public enum SoundType
        {
            BGMUSIC = 0, SOUND = 1
        }

        public enum SoundStatus
        {
            ACTIVE = 1, NON_ACTIVE = 0
        }
}
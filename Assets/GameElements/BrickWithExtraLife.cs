﻿namespace Arkanoid2D
{
    public class BrickWithExtraLife : Brick
    {
        private int lives = 2;

        public override void Damaged()
        {
            lives--;

            if(lives <=0)
            {
                Destroyed();
            }
        }

        void Start()
        {

        }

        void Update()
        {

        }

        public override void ResetElement()
        {
            base.ResetElement();
            lives = 2;
        }

        public override void Reward()
        {

            GameStateManager.instance.IncreaseScore(20);
        }

    }
}

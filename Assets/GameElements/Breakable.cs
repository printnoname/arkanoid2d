﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Arkanoid2D
{
    public interface Breakable
    {
        void Damaged();
        void Destroyed();
    }
}

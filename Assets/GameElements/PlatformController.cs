﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Arkanoid2D
{
    public class PlatformController : MonoBehaviour, Resetable
    {

        private Vector3 platformPosition;
        public float velocity;

        private Vector3 initialPosition;
        private Vector2 boundaries;

        void Start()
        {
            initialPosition = gameObject.transform.position;
            platformPosition = initialPosition;
            boundaries = getBoundaries();
        }

        void Update()
        {
            platformPosition.x += Input.GetAxisRaw("Horizontal") * velocity;

            transform.position = platformPosition;

            if (platformPosition.x < boundaries[0])
            {
                transform.position = new Vector3(boundaries[0], platformPosition.y, platformPosition.z);
            }

            if (platformPosition.x > boundaries[1])
            {
                transform.position = new Vector3(boundaries[1], platformPosition.y, platformPosition.z);
            }
        }

        public void setOriginalPosition()
        {
            platformPosition = initialPosition;
        }


        //Returns boundaries. First element is left boundary X value; Second element is rightBoundary X value;
        private Vector2 getBoundaries()
        {
            float platformSizeCompensations = getPlatformWidth() / 2;

            GameObject leftBoundary = GameObject.Find("LeftSideBarrier");
            Sprite leftBoundarySprite = leftBoundary.GetComponent<SpriteRenderer>().sprite;

            Vector3 leftBoundaryPosition = leftBoundary.transform.position;
            float leftBoundaryX = (leftBoundaryPosition + leftBoundarySprite.bounds.min)[0] + platformSizeCompensations;

            GameObject rightBoundary = GameObject.Find("RightSideBarrier");
            Sprite rightBoundarySprite = rightBoundary.GetComponent<SpriteRenderer>().sprite;

            Vector3 rightBoundaryPosition = rightBoundary.transform.position;
            float rightBoundaryX = (rightBoundaryPosition + rightBoundarySprite.bounds.max)[0] - platformSizeCompensations;

            return new Vector2(leftBoundaryX, rightBoundaryX);
        }


        private float getPlatformWidth()
        {
            SpriteRenderer sprite = gameObject.GetComponent<SpriteRenderer>();
            return sprite.bounds.size.x;
        }

        public void ResetElement()
        {
            platformPosition = initialPosition;
            velocity = 1.8f;
        }
    }
}

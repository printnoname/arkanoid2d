﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Arkanoid2D {
    public class Brick  : MonoBehaviour,Breakable,Resetable
    {
        private bool active = true;
        public virtual void Damaged()
        {
            Destroyed();
        }

        public void Destroyed()
        {
            GameObject[] bricks = GameObject.FindObjectsOfType<GameObject>();

            int counter = 0;

            for (int i = 0; i < bricks.Length; i++)
            {
                if (bricks[i].GetComponent<Brick>() != null)
                {
                    if (bricks[i].GetComponent<Brick>().active != false)
                        counter++;
                }            
            }

            if(counter<2)
            {
                GameStateManager.instance.SetPlayerWon();
            } else
            {
                active = false;
                gameObject.GetComponent<SpriteRenderer>().enabled = false;
                gameObject.GetComponent<BoxCollider2D>().enabled = false;
            }

            Reward();
        }

        
        void Start()
        {

        }

        
        void Update()
        {

        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag == "Ball")
            {
                AudioManager.instance.Play("hit-sound",AudioManager.SoundPlayType.DROP);
                Damaged();
            }
        }

        public virtual void Reward()
        {
            GameStateManager.instance.IncreaseScore(10);
        }

        public virtual void ResetElement()
        {
            active = true;
            gameObject.GetComponent<SpriteRenderer>().enabled = true;
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Arkanoid2D
{
    public class BallPhysicsScript : MonoBehaviour,Resetable
    {
        
        private Vector3 position;
        private bool inSpace;

        private float ballCompensation;
        private float bottomBoundary;

        public GameObject platform;
        private float initalPositionY;

        //Attribute for tracking platform's movements
        float platformPrevXValue;

        void Start()
        {
            inSpace = false;
            position = transform.position;
            initalPositionY = position.y + 0.1f;
            ballCompensation = getBallHeight();
            bottomBoundary = getBottomBoundary();
            platformPrevXValue = platform.transform.position.x;
        }

        void Update()
        {

            bool isPlatformMoving = false;

            if(platformPrevXValue != platform.transform.position.x)
            {
                isPlatformMoving = true;
            }

            if (Input.GetButtonDown("Jump") == true)
            {
                if (!inSpace)
                {
                    float xValueStartForce = 0.0f;
                    if(isPlatformMoving)
                    {
                        float diff = platform.transform.position.x - platformPrevXValue;
                        xValueStartForce = diff * -(5000.0f);
                    }

                    Vector2 startForce = new Vector2(xValueStartForce, 3000.0f);
                    gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
                    gameObject.GetComponent<Rigidbody2D>().AddForce(startForce);

                    
                    inSpace = !inSpace;
                }
                
            }

            if ((platform != null) && (!inSpace))
            {
                position.x = platform.transform.position.x;
                transform.position = position;
            }

           
            float bound = -(bottomBoundary + ballCompensation);
            
            if (inSpace && (transform.position.y.CompareTo(bound) < 0)) 
            {
                GameStateManager.instance.SetPlayerLost();
            }

            platformPrevXValue = platform.transform.position.x;
        }

        //Get bottom boundary Y value
        private float getBottomBoundary()
        {
            GameObject bottomBoundary = GameObject.Find("GameField");
            Sprite bottomBoundarySprite = bottomBoundary.GetComponent<SpriteRenderer>().sprite;

            Vector3 bottomBoundaryPosition = bottomBoundary.transform.position;

            float bottomBoundaryY = (bottomBoundaryPosition.y + (bottomBoundarySprite.bounds.max.y * bottomBoundary.transform.localScale.y));

            return bottomBoundaryY;
        }

        private float getBallHeight()
        {
            SpriteRenderer sprite = gameObject.GetComponent<SpriteRenderer>();
            return sprite.bounds.size.y;
        }

         //Fixing infinity bouncing
         void OnCollisionEnter2D(Collision2D collision)
        {
            GetComponent<Rigidbody2D>().velocity += new Vector2(Random.Range(0.0f, 5.0f), Random.Range(0.0f, 5.0f));
        }

        public void ResetElement()
        {
            inSpace = !inSpace;
            position.x = 0;
            position.y = initalPositionY;
            transform.position = position;
            platformPrevXValue = 0;
            gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);
        }
    }
}
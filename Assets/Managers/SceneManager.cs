﻿using UnityEngine;

namespace Arkanoid2D
{
    public class SceneManager : MonoBehaviour
    {
        public SceneSettingsData[] sceneSettingsData;
        private SceneSettingsData currentSceneSettings = null;

        public static SceneManager instance = null;

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }

            else if (instance != this)
            {
                Destroy(gameObject);
            }

            DontDestroyOnLoad(gameObject);

        }


        public void Load(string sceneName)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName);
            currentSceneSettings = null;

            foreach (SceneSettingsData sceneSettings in sceneSettingsData)
            {
                if (sceneSettings.sceneName.Equals(sceneName))
                {
                    currentSceneSettings = sceneSettings;
                }
            }

            if (currentSceneSettings != null)
            {
                AudioManager.instance.InitSceneAudio(currentSceneSettings.soundsData);
            }

            switch(sceneName)
            {
                case "MenuScene":
                    AudioManager.instance.Play("menu-music", AudioManager.SoundPlayType.DROP);
                    break;
                case "GameScene":
                    AudioManager.instance.Play("game-music", AudioManager.SoundPlayType.DROP);
                    break;
                default:
                    break;
                
            }
        }

    }
}
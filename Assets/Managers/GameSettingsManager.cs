﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;
using System.Collections;

namespace Arkanoid2D
{
    public class GameSettingsManager : MonoBehaviour
    {
        public static GameSettingsManager instance = null;

        private GameSettingsData gameSettingsData;

        const string folderName = "settings";

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }

            else if (instance != this)
            {
                Destroy(gameObject);
            }

            DontDestroyOnLoad(gameObject);

        }

        public void Init()
        {
            if (!DoesSettingsFileExist())
            {
                gameSettingsData = CreateSettingsFile();
            }
            else
            {
                gameSettingsData = LoadSettings();
            }

            AudioManager.instance.InitSoundSettings(gameSettingsData);
        }

        private GameSettingsData LoadSettings()
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            string path = Path.Combine(Application.persistentDataPath, Path.Combine(folderName, "settings.dat"));

            using (FileStream fileStream = File.Open(path, FileMode.Open))
            {
                return (GameSettingsData)binaryFormatter.Deserialize(fileStream);
            }
        }

        private GameSettingsData CreateSettingsFile()
        {
            GameSettingsData gameSettingsDataTemp = new GameSettingsData();

            BinaryFormatter binaryFormatter = new BinaryFormatter();

            if (!Directory.Exists(Path.Combine(Application.persistentDataPath, folderName)))
            {
                Directory.CreateDirectory(Path.Combine(Application.persistentDataPath, folderName));
            }


            using (FileStream fileStream = File.Open(Path.Combine(Application.persistentDataPath, Path.Combine(folderName, "settings.dat")), FileMode.OpenOrCreate))
            {
                binaryFormatter.Serialize(fileStream, gameSettingsDataTemp);
            }

            return gameSettingsDataTemp;
        }

        private Boolean DoesSettingsFileExist()
        {
            return File.Exists(Path.Combine(Application.persistentDataPath, Path.Combine(folderName, "settings.dat")));
        }

        public void ChangeSoundSettings(SoundType type, float volume)
        {
            StartCoroutine(ChangeSoundSettingsCo(type, volume));
        }

        private void WriteNewSettings(GameSettingsData newGameSettingsData)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();

            using (FileStream fileStream = File.Open(Path.Combine(Application.persistentDataPath, Path.Combine(folderName, "settings.dat")), FileMode.Truncate))
            {
                binaryFormatter.Serialize(fileStream, newGameSettingsData);
            }
        }

        IEnumerator ChangeSoundSettingsCo(SoundType type, float volume)
        {
            switch (type)
            {
                case SoundType.BGMUSIC:
                    this.gameSettingsData.musicVolume = volume;
                    break;
                case SoundType.SOUND:
                    this.gameSettingsData.soundEffectsVolume = volume;
                    break;
                default:
                    break;
            }

            WriteNewSettings(this.gameSettingsData);

            yield return null;
        }

        public float GetMusicVolume()
        {
            return this.gameSettingsData.musicVolume;
        }

        public float GetSFXVolume()
        {
            return this.gameSettingsData.soundEffectsVolume;
        }
    }

}
﻿using UnityEngine.Audio;
using UnityEngine;
using System;

namespace Arkanoid2D
{
    public class AudioManager : MonoBehaviour
    {

        private float sFXVolume;
        private float musicVolume;

        private Sound[] sceneSounds;

        public static AudioManager instance = null;

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }

            else if (instance != this)
            {
                Destroy(gameObject);
            }

            DontDestroyOnLoad(gameObject);

        }

        public void InitSoundSettings(GameSettingsData gameSettingsData)
        {
            musicVolume = gameSettingsData.musicVolume;
            sFXVolume = gameSettingsData.soundEffectsVolume;
        }

        public void InitSceneAudio(SoundData[] soundData)
        {
            if (sceneSounds == null)
            {
                sceneSounds = new Sound[soundData.Length];

                for (int i = 0; i < soundData.Length; i++)
                {
                    sceneSounds[i] = new Sound(soundData[i]);
                }
            }
            else
            {
                int originalSceneSoundsLength = sceneSounds.Length;
                Sound[] newSceneSounds = new Sound[soundData.Length];

                for (int i = 0; i < soundData.Length; i++)
                {
                    newSceneSounds[i] = new Sound(soundData[i]);
                }
                Array.Resize<Sound>(ref sceneSounds, originalSceneSoundsLength + newSceneSounds.Length);
                Array.Copy(newSceneSounds, 0, sceneSounds, originalSceneSoundsLength, newSceneSounds.Length);
            }

        }

        public void ChangeVolume(float volume, SoundType type)
        {
            switch (type)
            {
                case SoundType.BGMUSIC:
                    musicVolume = volume;
                    break;
                case SoundType.SOUND:
                    sFXVolume = volume;
                    break;
                default:
                    break;
            }

            Sound[] soundsForChange = Array.FindAll<Sound>(sceneSounds, sound => sound.status == SoundStatus.ACTIVE && sound.type == type);

            foreach (Sound i in soundsForChange)
            {
                switch (type)
                {

                    case SoundType.BGMUSIC:
                        foreach (AudioSource s in i.sources)
                        {
                            s.volume = i.volume * musicVolume;
                        }
                        break;
                    case SoundType.SOUND:
                        foreach (AudioSource s in i.sources)
                        {
                            s.volume = i.volume * sFXVolume;
                        }
                        break;
                    default:
                        break;
                }



            }

        }

        public void Play(string soundName, SoundPlayType type)
        {
            Sound playSound = null;
            AudioSource source = null;

            foreach (Sound e in sceneSounds)
            {
                if (e.name.Equals(soundName))
                {
                    playSound = e;
                }
            }

            if (playSound != null)
            {
                if (playSound.type == SoundType.BGMUSIC)
                {
                    Sound currentlyPlayingMusic = Array.Find<Sound>(sceneSounds, sound => sound.type == SoundType.BGMUSIC && sound.status == SoundStatus.ACTIVE);
                    if (currentlyPlayingMusic != null)
                    {
                        currentlyPlayingMusic.status = SoundStatus.NON_ACTIVE;
                        StopSound(currentlyPlayingMusic, SoundStopType.NOFADE);
                    }
                }



                if (playSound.sources == null || playSound.sources.Length < 1)
                {
                    playSound.sources = new AudioSource[1];
                    playSound.sources[0] = gameObject.AddComponent<AudioSource>();
                    playSound.sources[0].name = playSound.clip.name;
                    playSound.sources[0].clip = playSound.clip;

                    switch (playSound.type)
                    {
                        case SoundType.BGMUSIC:
                            playSound.sources[0].volume = playSound.volume * musicVolume;
                            break;
                        case SoundType.SOUND:
                            playSound.sources[0].volume = playSound.volume * sFXVolume;
                            break;
                        default:
                            break;
                    }

                    playSound.sources[0].pitch = playSound.pitch;
                    source = playSound.sources[0];

                } else {

                    foreach(AudioSource _source in playSound.sources)
                    {

                        if(_source.clip.name.Equals(playSound.clip.name))
                        {
                            source = _source;
                        }
                    }

                    if (!source)
                    {
                        int originalSoundSourcesLength = playSound.sources.Length;

                        Array.Resize<AudioSource>(ref playSound.sources, originalSoundSourcesLength + 1);
                        playSound.sources[originalSoundSourcesLength] = gameObject.AddComponent<AudioSource>();
                        playSound.sources[originalSoundSourcesLength].name = playSound.clip.name;
                        playSound.sources[originalSoundSourcesLength].clip = playSound.clip;

                        switch (playSound.type)
                        {
                            case SoundType.BGMUSIC:
                                playSound.sources[originalSoundSourcesLength].volume = playSound.volume * musicVolume;
                                break;
                            case SoundType.SOUND:
                                playSound.sources[originalSoundSourcesLength].volume = playSound.volume * sFXVolume;
                                break;
                            default:
                                break;
                        }

                        playSound.sources[originalSoundSourcesLength].pitch = playSound.pitch;
                        source = playSound.sources[originalSoundSourcesLength];
                    }
                }

                

                playSound.status = SoundStatus.ACTIVE;


                if (type == SoundPlayType.DROP)
                    source.PlayOneShot(playSound.clip);
                else
                    source.Play();
            }
            else
            {
                Debug.Log("No sound found");
            }
        }

        public void Stop(string soundName)
        {
            Sound soundsStop = Array.Find<Sound>(sceneSounds, sound => sound.name == soundName);
            StopSound(soundsStop, SoundStopType.NOFADE);
        }

        public void StopSound(Sound sound, SoundStopType type)
        {
            if (type == SoundStopType.NOFADE)
            {
                foreach (AudioSource i in sound.sources)
                {
                    i.Stop();
                    Destroy(i);
                }
            }

        }

        public enum SoundPlayType
        {
            DROP = 0, SUSTAIN = 1
        }

        public enum SoundStopType
        {
            NOFADE = 0, FADE = 1
        }

    }
}
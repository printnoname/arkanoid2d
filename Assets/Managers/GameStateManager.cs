﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Arkanoid2D
{
    public class GameStateManager : MonoBehaviour
    {
        public static GameStateManager instance = null;

        private bool win = false;
        private bool lose = false;
        private bool gameFinished = false;
        private int score = 0;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }

            else if (instance != this)
            {
                Destroy(gameObject);
            }

            DontDestroyOnLoad(gameObject);
        }

        void Start()
        {

        }

        void Update()
        {
            if(!gameFinished)
            {
                if (instance.win)
                {
                    instance.gameFinished = true;
                    IfWon();
                }

                else if (instance.lose)
                {
                    instance.gameFinished = true;
                    IfLost();
                }
            }
        }


        public void SetPlayerWon()
        {
            instance.win = true;
        }

        public void SetPlayerLost()
        {
            instance.lose = true;
        }

        // Действия при начале игры
        public void RestartGame()
        {
            instance.gameFinished = false;
            instance.win = false;
            instance.lose = false;
            instance.score = 0;

            GameObject[] objects = GameObject.FindObjectsOfType<GameObject>();

            for (int i = 0; i < objects.Length; i++)
            {
                if((objects[i].GetComponent<Resetable>()!=null))
                {
                    objects[i].GetComponent<Resetable>().ResetElement();
                }
            }
        }

        // Действия при победе
        private void IfWon()
        {
            GameObject.FindObjectOfType<MenuFunctions>().ShowWinMenu();
            AudioManager.instance.Play("win-sound", AudioManager.SoundPlayType.SUSTAIN);
        }

        // Действия при проигрыше
        private void IfLost()
        {
            GameObject.FindObjectOfType<MenuFunctions>().ShowLoseMenu();
            AudioManager.instance.Play("lose-sound", AudioManager.SoundPlayType.SUSTAIN);
        }

        public void IncreaseScore(int points)
        {
            instance.score += points;
        }


        public bool IsGameFinished()
        {
            return instance.gameFinished;
        }

        public int GetScore()
        {
            return instance.score;
        }
    }
}

﻿using UnityEngine;

namespace Arkanoid2D {
    public class SystemManager : MonoBehaviour
    {
        public static SystemManager instance = null;

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }

            else if (instance != this)
            {
                Destroy(gameObject);
            }

            DontDestroyOnLoad(gameObject);

        }

        void Start()
        {
            GameSettingsManager.instance.Init();
            SceneManager.instance.Load("MenuScene");
        }
    }
}
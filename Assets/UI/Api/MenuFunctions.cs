﻿using UnityEngine;
using Arkanoid2D;

public class MenuFunctions : MonoBehaviour {
    public GameObject winMenu = null;
    public GameObject loseMenu = null;
    void Start()
    {
      
    }

    public void StartGame()
    {
        SceneManager.instance.Load("GameScene");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ToMainMenu()
    {
        SceneManager.instance.Load("MenuScene");
    }

    public void BackToGame()
    {
            GameObject pauseMenuObject = GameObject.Find("PauseMenu");
            pauseMenuObject.SetActive(false);

            PauseMenuScript.GameIsPaused = false;
            AudioListener.pause = false;
            Time.timeScale = 1f;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;

    }

    public void ShowWinMenu()
    {
        winMenu.SetActive(true);
    }


    public void ShowLoseMenu()
    {
        loseMenu.SetActive(true);
    }

    public void MusicVolumeSliderChange(float volume)
    {
        AudioManager.instance.ChangeVolume(volume,SoundType.BGMUSIC);
        GameSettingsManager.instance.ChangeSoundSettings(SoundType.BGMUSIC, volume);
    }
    public void SFXSliderChange(float volume)
    {
        AudioManager.instance.ChangeVolume(volume, SoundType.SOUND);
        GameSettingsManager.instance.ChangeSoundSettings(SoundType.SOUND, volume);
    }

    public void RestartGame()
    {
        GameStateManager.instance.RestartGame();
    }
}

﻿using UnityEngine;
using Arkanoid2D;

public class PauseMenuScript : MonoBehaviour {

    public static bool GameIsPaused = false;
    public GameObject pauseMenu;
    
    void Start () {

    }
	
	void Update () {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (!GameStateManager.instance.IsGameFinished())
            {

                if (PauseMenuScript.GameIsPaused)
                {
                    pauseMenu.SetActive(false);
                    PauseMenuScript.GameIsPaused = false;
                    Time.timeScale = 1f;
                    Cursor.visible = false;
                    Cursor.lockState = CursorLockMode.Locked;

                }
                else
                {
                    pauseMenu.SetActive(true);
                    PauseMenuScript.GameIsPaused = true;
                    Time.timeScale = 0f;
                    Cursor.visible = true;
                    Cursor.lockState = CursorLockMode.None;

                }
            }
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Arkanoid2D
{
    public class SliderInit : MonoBehaviour
    {
        public SoundType type;

        void Start()
        {
                Slider slider = gameObject.GetComponent<Slider>();

                switch (type)
                {
                case SoundType.BGMUSIC:
                    slider.value = GameSettingsManager.instance.GetMusicVolume();
                    break;
                case SoundType.SOUND:
                    slider.value = GameSettingsManager.instance.GetSFXVolume();
                    break;
            }
            
        }
    }
}


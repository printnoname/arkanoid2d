﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Arkanoid2D
{
    public class ScoreText : MonoBehaviour
    {
        void Start()
        {

        }

        void Update()
        {
            gameObject.GetComponent<UnityEngine.UI.Text>().text = "Счёт : " + GameStateManager.instance.GetScore().ToString();
        }
    }
}

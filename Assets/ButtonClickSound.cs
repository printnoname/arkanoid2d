﻿using UnityEngine;
using UnityEngine.UI;

namespace Arkanoid2D {
    public class ButtonClickSound : MonoBehaviour
    {
        void Awake()
        {
            gameObject.GetComponent<Button>().onClick.AddListener(
                delegate () {
                    AudioManager.instance.Play("click-sound", AudioManager.SoundPlayType.DROP);
                }
           );
        }
    }
}
